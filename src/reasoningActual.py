# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 11:19:13 2018

@author: svten
"""

facts = []
relations = ["student", "graduated", "employed", "major", "department", 
             "fast", "belongsto", "computer", "takes", "graduateSchool", "breadboard", "coreCourse"]
rMap = {}
rules = {}

for index, el in enumerate(relations):
    rMap[el] = index


class Rule:
    
    def __init__(self, conclusion, antecedent):
        self.antecedent = antecedent
        self.conclusion = conclusion
        
        global facts, rMap, relations
        
        if conclusion.split("(")[0] not in relations:
            relations.append(conclusion.split("(")[0])
            rMap[conclusion.split("(")[0]] = len(relations) - 1
            facts.append(set())
    
    #returns LHS of rule with the correct arguments
    def mapRule(self, query): #given RHS, map to argument of LHS
        par1 = self.conclusion.find("(")
        par2 = self.conclusion.find(")")
        argC = self.conclusion[par1 + 1:par2] #get value between parentheses
        par1 = query.find("(")
        par2 = query.find(")")
        newArg = query[par1 + 1:par2]
        newQuery = self.antecedent
        
        subquery = argC.split(",")
        values = newArg.split(",")
        for ind in range(0,len(subquery)):
            newQuery = newQuery.replace(subquery[ind], values[ind])
        return newQuery
        

#checks if word is in knowledge base given index in list
def isTrue(ind, word):
    #return word in facts[ind]
    if word in facts[ind]:
        return True
    else:
        return False
         

# separate query into parts and analyze one part a time.
# if one part is false, then stop and return false. else keep going until 
# resolve all atoms
def parseQuery(query):  
    currentValue = True
    #op = "and"
    query = query.split(" ") #split into parts. ex. student(s), and, etc.
    for phrase in query: 
       
        if(phrase.find("(") != -1):
            #print(phrase)
            subquery = phrase.split("(")
            inKnowledgeBase = False
           
            if(subquery[0] in rMap):
                inKnowledgeBase = isTrue(rMap[subquery[0]], subquery[1].split(")")[0])
            if (inKnowledgeBase == False):
                #print(subquery[0])
                isRuleTrue = False
                if (subquery[0] in rules):
                    for rule in rules[subquery[0]]:
                        ruleQuery = rule.mapRule(phrase)
                        #print(ruleQuery)
                        isRuleTrue = parseQuery(ruleQuery)
                        if (isRuleTrue):
                            break
                #started adding from here
                elif(subquery[0] == "equals"):
                    leftArg,rightArg = subquery[1].split(")")[0].split(",")
                    if leftArg == rightArg:
                        isRuleTrue = True
                #ends here
                currentValue = currentValue and isRuleTrue
            else:
                currentValue = currentValue and inKnowledgeBase #might not need this branch
            if(currentValue == False):
                #check if it is on the RHS of a rule
                return False
        #return False
        #elif phrase == "and" or phrase == "or":
            #op = phrase
    return currentValue

def getRuleName(query):
    return query.split("(")[0]

def parseInput(query):
    #if query[:1] == "?": #parse query
    if query[-1:]  == "?":   #checks if ends with question mark
        return parseQuery(query[:-1])
        #return parseQuery(query[1:])
    elif query.find("<-") != -1: #rule is being added
        loc = query.find("<-")
        ruleName = getRuleName(query)
        if (ruleName in rules):
            rules[ruleName].add(Rule(query[0:loc - 1], query[loc+2:len(query)]))
        else:
            rules[ruleName] = {Rule(query[0:loc - 1], query[loc+2:len(query)])} #conclusion = antecedent
        return "Rule has been added"
    else: # parse knowledge to be added
        knowledge = query.split("(") 
        data = knowledge[1].split(")")[0]
        if knowledge[0] in relations: #rule exists
            
            facts[rMap[knowledge[0]]].add(data)  #uses value from map to add value to correct list in facts
            return data + " has been added to " + knowledge[0]
        else: 
            relations.append(knowledge[0])
            rMap[knowledge[0]] = len(relations) - 1
            facts.append(set())
            facts[rMap[knowledge[0]]].add(data)
            return data + " has been added to " + knowledge[0] + " and relation created"
            #change to add the relation
            #print("Rule does not exist to add this knowledge")
        #print(query)


def main():
    #rules["takes"] = {Rule("takes(:s:,:c:)", "coreCourse(:c:) and graduated(:s:) and student(:s:)")}
# =============================================================================
#     rules["employed"] = {Rule("employed(:s:)", "graduated(:s:) and student(:s:)")}
#     rules["graduateSchool"] = {Rule("graduateSchool(:s:)", "graduated(:s:) and student(:s:)")}
#     #rules["takes"].add(Rule("takes(:s:,:c:)", "student(:s:) and department(:s:,ECE)"))
#     rules["department"] = {Rule("department(:s:,ECE)", "major(:s:,EE) and student(:s:)")}
#     rules["department"].add(Rule("department(:s:,ECE)", "major(:s:,SE) and student(:s:)"))
#     rules["department"].add(Rule("department(:s:,ECE)", "major(:s:,CE) and student(:s:)"))
#     rules["department"].add(Rule("department(:s:,ECE)", "major(:s:,AE) and student(:s:)"))
#     rules["major"] = {Rule("major(:s:,SE)", "takes(:s:,ECE318) and department(:s:,ECE) and student(:s:)")}
#     rules["major"].add(Rule("major(:s:,CE)", "takes(:s:,ECE318) and department(:s:,ECE) and student(:s:)"))
#     rules["major"].add(Rule("major(:s:,EE)", "takes(:s:,ECE336) and department(:s:,ECE) and student(:s:)"))
#     rules["major"].add(Rule("major(:s:,AE)", "takes(:s:,ECE336) and department(:s:,ECE) and student(:s:)"))
     
    while(True):
        query = input("Enter knowledge or query: ")
        if query == "q":
            break;
        result = parseInput(query)
        print(result)
        if(result == True):
            knowledge = query.split("(") 
            data = knowledge[1].split(")")[0]
            facts[rMap[knowledge[0]]].add(data)
        
        #print(query)
        
    
  
if __name__ == "__main__":
    # calling main function
    for r in relations:
        facts.append(set())

    rules["takes"] = {Rule("takes(:s:,:c:)", "prereq(ECE118,:c:) and takes(:s:,ECE118)")}
    prereqs = ["ECE322", "ECE218"]
    for classCode in prereqs:
        rules["takes"].add(Rule("takes(:s:,:c:)", "prereq(" + classCode + ",:c:) and takes(:s:," + classCode + ")"))
   
    main()   