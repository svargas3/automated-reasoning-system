'''
Created on May 8, 2018

@author: svten
'''
import boto3
import json
import urllib.parse

dynamo = boto3.resource('dynamodb')
rulesTable = dynamo.Table('Rules')
factsTable = dynamo.Table('Facts')

#add to dynamodb
def addRule(query):
    loc = query.find("<-")
    ruleName = query.split("(")[0]
    rhs = query[loc+3:len(query)]
    par1 = query.find("(")
    par2 = query.find(")")
    params = query[par1 + 1:par2]
    #search dynamodb for rule
    item = rulesTable.get_item(Key = {'LHS': ruleName}) #{'S': ruleName}})
    print(item)
    if('Item' not in item): #item == {}): #create item if doesn't exist
        response = rulesTable.put_item( Item={'LHS':ruleName, 'params': params, 'RHS':[rhs]})
        item = rulesTable.get_item(Key = {'LHS': ruleName}) #should exist now
    if('RHS' in item['Item'].keys()):
        #rules = item['RHS']
        if(rhs not in item['Item']['RHS']): #.values()):
            response = rulesTable.update_item( Key={'LHS':ruleName},
                                           UpdateExpression="SET RHS= list_append(RHS,:r)",
                                           ExpressionAttributeValues={':r': [rhs]},
                                           ReturnValues="UPDATED_NEW")
    else:
        response = rulesTable.update_item( Key={'LHS':ruleName},
                                           UpdateExpression="SET RHS=:r",
                                           ExpressionAttributeValues={':r': [rhs]},
                                           ReturnValues="UPDATED_NEW")
    #print(response)
    return "Rule has been added"

#add to dynamodb
def addFact(query):
    knowledge = query.split("(") 
    relation = knowledge[0]
    data = knowledge[1].split(")")[0]
    
    item = factsTable.get_item(Key = { 'relation': relation}) #'knowledge': data})
    if('Item' not in item): #item == {}): #not found
        print("creating item")
        response = factsTable.put_item( Item={'relation': relation, 'knowledge': [data]})
        item = factsTable.get_item(Key = { 'relation': relation})
    if('knowledge' in item['Item'].keys()):
        if(data not in item['Item']['knowledge']): #.values()):
            response = factsTable.update_item( Key={'relation': relation},
                                           UpdateExpression="SET knowledge= list_append(knowledge,:d)",
                                           ExpressionAttributeValues={':d': [data]},
                                           ReturnValues="UPDATED_NEW")
    return data + " has been added to " + knowledge[0] + " and relation created"
    
def isTrue(relation, knowledge):
    #check if word is in facts
    item = factsTable.get_item(Key = { 'relation': relation})
    if knowledge in item['Item']['relation'].values():
        return True
    else:
        return False

#returns LHS of rule with the correct arguments
#def mapRule(query): #given RHS, map to argument of LHS
    #item = rulesTable.get_item(Key = {'LHS': ruleName})
    #argC = item['Item']['params']
    #par1 = self.conclusion.find("(")
    #par2 = self.conclusion.find(")")
    #argC = self.conclusion[par1 + 1:par2] #get value between parentheses
    #par1 = query.find("(")
    #par2 = query.find(")")
    #newArg = query[par1 + 1:par2]
    #newQuery = self.antecedent
    
    #subquery = argC.split(",")
    #values = newArg.split(",")
    #for ind in range(0,len(subquery)):
     #   newQuery = newQuery.replace(subquery[ind], values[ind])
    #return newQuery
    
#checks dynamodb to see if exists. returns true or false
def queryFacts(query):
    currentValue = True
    query = query.split(" ") #split into parts. ex. student(s), and, etc.
    for phrase in query: 
       
        if(phrase.find("(") != -1):
            #print(phrase)
            subquery = phrase.split("(")
            inKnowledgeBase = False
           
            inKnowledgeBase = isTrue(subquery[0], subquery[1].split(")")[0])
            if (inKnowledgeBase == False):
                #print(subquery[0])
                isRuleTrue = False
                rules = rulesTable.get_item(Key = {'LHS': subquery[0]})
                
                if(subquery[0] == "equals"):
                    leftArg,rightArg = subquery[1].split(")")[0].split(",")
                    if leftArg == rightArg:
                        isRuleTrue = True
                else:
                    for rule in rules['Item']['RHS']:
                        ruleQuery = rule.mapRule(phrase) #fix this
                        #print(ruleQuery)
                        isRuleTrue = queryFacts(ruleQuery)
                        if (isRuleTrue):
                            break
                
                currentValue = currentValue and isRuleTrue
            else:
                currentValue = currentValue and inKnowledgeBase #might not need this branch
            if(currentValue == False):
                #check if it is on the RHS of a rule
                return False
        #return False
        #elif phrase == "and" or phrase == "or":
            #op = phrase
    return currentValue

def handler(event, context):

    statusCode = 200
    data = {}
    method = event['httpMethod']

    if method == 'POST':
        body = event['body']
        path = event['path']
        bdata = urllib.parse.parse_qs(body)
        
        if(path == '/addRule'):
            data = {'result': addRule(bdata['query'][0]) }
        elif(path == '/addFact'):
            data = {'result': addFact(bdata['query'][0]) }
        #else:
        #    data = queryFacts(bdata['query'][0])

    else:
        statusCode = 404
        data = {'mess' : 'bad httpMethod'}
    

    return {'statusCode': statusCode,
           'body': json.dumps(data),
            'headers': {'Content-Type': 'application/json'}}
    
    
