import json
import urllib.parse
#import os
#os.chmod("/add", 0o755)

def handler(event, context):

    statusCode = 200
    data = {}
    method = event['httpMethod']
    if method == 'GET':
        op1 = int(event['queryStringParameters']['op1'])
        op2 = int(event['queryStringParameters']['op2'])
        ans = op1 + op2
        data = {'answer' : str(ans)}
    elif method == 'POST':
        body = event['body']
        bdata = urllib.parse.parse_qs(body)
        op1 = int(bdata['op1'][0])
        op2 = int(bdata['op2'][0])
        ans = op1 + op2
        data = {'answer' : str(ans)}
    else:
        statusCode = 404
        data = {'mess' : 'bad httpMethod'}
    

    return {'statusCode': 200,
           'body': json.dumps(data),
            'headers': {'Content-Type': 'application/json'}}


